1. Ruby version 2.5.3 required.

2. Before running the application make sure you have Redis installed on your machine and running on port `6379`.
Alternatively, you can simply download the official Redis docker image and run a container with properly mapped ports using command:
```docker run -p 6379:6379 redis```
(there is no need to have Redis up while running the specs as fakeredis gem is used in the test environment)

3. Clone the repository and ```bundle install```

4. Running the server:
```rails s```

5. Advice for manual testing: best open two browser windows next to each other, one in the normal mode and one in the incognito mode. Then refresh pages and check how does it work.

6. There are two types (levels) of rate limiters implemented currently. The first one is responsible for handling IP address rate limitation and the other one is handling session ID rate limitation, which also takes into account IP address in order to avoid collisions from users from different networks. These rate limiters work on the different levels. It means that the current mechanism counts all the requests for particular rate limiters and perform banning separetly for each rate limiter, so in that case when you have IP address banned you will be able to ban your session ID in the same time independently.

7. Properties of the rate limiting and banning mechanism can be easily configured by updating values in config/limiting_config.yml file. Moreover, there is a possibility to enable/disable each rate limiting and banning type (level). RSpec tests use configuration placed in spec/fixtures/limiting_config.yml.

8. Specs can be simply run by typping:
```rspec```

9. Rubocop:
```rubocop```

10. Thoughts:
* There was an idea that there could be also used JavaScript for making requests to the server providing additional informations about a given client, like a language of the web browser, a web browser's window size (for fullscreen it's pretty much the same all the time), information about timezone, geolocation, web browser version etc. Generally, all the things we can get  about client among others from https://developer.mozilla.org/en-US/docs/Web/API/Navigator that could be useful for creating more types (levels) of rate limiters.
* There is an alternative storage that could be used for storing information about blocked traffic - RocksDB. I read that this is a very high performance and optimized for fast storage database. However, for the sake of this test task implementation I decided to use Redis as I'm more familiar with it and it offers everything I needed for getting it done.
