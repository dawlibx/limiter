# frozen_string_literal: true

# LandingPageController class.
class LandingPageController < ApplicationController
  def index
    flash[:notice] = RateLimitersCallerService.new.call(request.remote_ip, session.id)
  end
end
