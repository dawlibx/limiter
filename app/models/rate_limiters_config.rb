# frozen_string_literal: true

# Rate limiters global configuration.
class RateLimitersConfig
  include Singleton

  # File path to configuration for all the rate limiters.
  CONFIG_FILE_PATH = "#{Rails.root}/config/limiting_config.yml"

  attr_reader :config

  # Initializes config variable with a whole configuration.
  def initialize
    # Limiting and banning mechanism settings
    @config = RecursiveOpenStruct.new(
      YAML.load(
        File.read(CONFIG_FILE_PATH)
      ).with_indifferent_access
    )

    freeze
  end

  # @return [Array<Symbol>] returns all types of the available rate limiters listed in the config
  def types
    config.to_h.keys
  end
end
