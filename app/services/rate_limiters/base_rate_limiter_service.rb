# frozen_string_literal: true

# RateLimiters module contains all the rate limiting and banning logic.
module RateLimiters
  # BaseRateLimiterService class is a base class that includes main logic which all subclasses use.
  class BaseRateLimiterService
    # Limitation for the size of a created ID.
    ID_LIMIT = 10
    # Prefix for ID's that are banned in DB.
    BANNED_ID_PREFIX = 'banned'

    # Freezing initializer so we can ensure there will be no unneeded instance variables created.
    def initialize
      freeze
    end

    private

    # @param id [String] an ID for which there is limitation performed
    # @param banned_id [String] a banned ID which is an auxiliary variable needed for storing
    #   information about banned ID in the database
    # @param config [RecursiveOpenStruct] a config for a concrete rate limiter of a given type
    #   that includes all the necessary properties for a given mechanism,
    #   e.g. a number of allowed requests for a given rate limiter, a time range during which
    #   there can be requests made before a given ID will be banned and the time of banning
    # @return [Integer, NilClass] returns number of requests already made for a given ID and
    #   for which limitation succeed or nil when a given ID is already banned
    # @note banning_time cannot be a smaller number than time_range because otherwise
    #   there could be a situation when a given ID is not banned and has an unacceptable
    #   number of requests assigned which will break the mechanism.
    def call(id, banned_id, config)
      return if Redis.current.get(banned_id)

      current_requests_number = Redis.current.get(id)

      if current_requests_number
        current_requests_number = Redis.current.incr(id).to_i

        if current_requests_number >= config.allowed_requests_number
          Redis.current.set(banned_id, 1)
          Redis.current.expire(banned_id, config.banning_time)
        end
      else
        current_requests_number = 1

        Redis.current.set(id, current_requests_number)
        Redis.current.expire(id, config.time_range)
      end

      current_requests_number
    end

    # @param string [String] a string we want to make a fingerprinted ID from with a limited length
    #   by ID_LIMIT constant
    # @return [String] a fingerprinted ID
    def fingerprint_id(string)
      Digest::SHA1.hexdigest(string)[0..ID_LIMIT]
    end

    # @param string [String] a string we want to make a fingerprinted banned ID from with a limited
    #   length by ID_LIMIT constant
    # @return [String] a fingerprinted ID
    def fingerprint_banned_id(string)
      fingerprint_id(BANNED_ID_PREFIX + string)
    end
  end
end
