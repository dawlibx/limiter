# frozen_string_literal: true

module RateLimiters
  # IpAddressRateLimiterService is a subclass of the BaseRateLimiterService base class and
  # it's responsible for handling cases when there is limiting and banning performed only
  # based on a given IP address.
  class IpAddressRateLimiterService < BaseRateLimiterService
    # @param args [Hash] the rate limiter input data
    # @option args [String] :ip_address IP address from which request was made, e.g. '127.0.0.1'
    # @return [Integer] already made number of requests from a given IP address
    def call(args)
      ip_address_id = fingerprint_id(args[:ip_address])
      banned_ip_address_id = fingerprint_banned_id(args[:ip_address])

      super(ip_address_id, banned_ip_address_id, RateLimitersConfig.instance.config.ip_address)
    end
  end
end
