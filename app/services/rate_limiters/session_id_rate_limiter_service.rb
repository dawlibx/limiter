# frozen_string_literal: true

module RateLimiters
  # SessionIdRateLimiterService is a subclass of the BaseRateLimiterService base class and
  # it's responsible for handling cases when there is limiting and banning performed based on
  # a given IP address and session ID combined.
  class SessionIdRateLimiterService < BaseRateLimiterService
    # @param args [Hash] the rate limiter input data
    # @option args [String] :ip_address IP address from which request was made, e.g. '127.0.0.1'
    # @option args [String] :session_id session ID from which request was made,
    #   e.g. '444444aaaa4444444444aaaa44444444'
    # @return [Integer] already made number of requests from a given IP address and
    #   session ID combined
    # @note There is a case when the request is made for the first time from a given web browser
    #   and that time there is no session ID provided, so we are handling it by returning
    #   zero value as session ID requests number then.
    def call(args)
      session_id_requests_number = 0

      if args[:session_id]
        session_id_id = fingerprint_id(args[:ip_address] + args[:session_id])
        banned_session_id_id = fingerprint_banned_id(args[:ip_address] + args[:session_id])

        session_id_requests_number = super(
          session_id_id, banned_session_id_id, RateLimitersConfig.instance.config.session_id
        )
      end

      session_id_requests_number
    end
  end
end
