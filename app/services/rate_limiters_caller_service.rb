# frozen_string_literal: true

# Service responsible for calling rate limiter services and handling output from them.
class RateLimitersCallerService
  # String that joins elements from array that includes rate limiter types.
  JOIN_ARG = ' & '
  # Arguments for #tr method using on rate limiter type names.
  TR_ARGS = ['_', ' '].freeze

  # Freezing initializer so we can ensure there will be no unneeded instance variables created.
  def initialize
    freeze
  end

  # @param ip_address [String] IP addres from which request was made, e.g. '127.0.0.1'
  # @param session_id [String] session ID from which request was made,
  #   e.g. '444444aaaa4444444444aaaa44444444'
  # @return [String] includes information about current status of the rate limitation,
  #   e.g. "You have 8 IP ADDRESS type & 5 SESSION ID type requests left yet."
  # @note Note that the current mechanism counts all the requests for particular rate limiters and
  #   perform banning separetly for each rate limiter, so in that case when you have your
  #   IP address banned you will be able to ban your session ID in the same time independently.
  def call(ip_address, session_id)
    RateLimitersConfig.instance.types.map.with_object({}) do |rate_limiter_type, request_numbers|
      if RateLimitersConfig.instance.config.send(rate_limiter_type).enabled
        request_numbers[rate_limiter_type] = rate_limiter(rate_limiter_type).call(
          ip_address: ip_address, session_id: session_id
        )
      end
    end.yield_self do |request_numbers|
      response(request_numbers)
    end
  end

  private

  # @param request_numbers [Hash] includes information about number of already performed requests
  #   for a particular rate limiter or an information about banning when there is nil instead,
  #   e.g. { ip_address: 5, :session_id: nil}
  # @return [String] status about limiting or banning
  def response(request_numbers)
    return limiting_status(request_numbers) if request_numbers.values.all?

    banning_status(request_numbers)
  end

  # @param request_numbers [Hash]
  # @return [String] status about limiting,
  #   e.g. 'You have 9 IP ADDRESS & 5 SESSION ID type requests left yet.'
  def limiting_status(request_numbers)
    request_numbers.map do |rate_limiter_type, requests_made_number|
      requests_left_number = RateLimitersConfig.instance.config.send(rate_limiter_type)
                                               .allowed_requests_number - requests_made_number

      "#{requests_left_number} #{rate_limiter_type_name(rate_limiter_type)}"
    end.yield_self do |status|
      status_message(status, :limiting_status)
    end
  end

  # @param request_numbers [Hash]
  # @return [String] status about limiting,
  #   e.g. 'You have 9 IP ADDRESS & 5 SESSION ID type requests left yet.'
  def banning_status(request_numbers)
    request_numbers.map do |rate_limiter_type, requests_made_number|
      rate_limiter_type_name(rate_limiter_type) unless requests_made_number
    end.yield_self do |status|
      status_message(status, :banning_status)
    end
  end

  # @param status [Array] e.g. ['9 IP ADDRESS', '5 SESSION ID']
  # @param status_type [Symbol] e.g. :limiting_status, :banning_status
  # @return [String]
  def status_message(status, status_type)
    status.compact.join(JOIN_ARG).yield_self do |joined_status|
      I18n.t("limiter.#{status_type}", status_type => joined_status)
    end
  end

  # @param rate_limiter_type [Symbol] type of a rate limiter we want to create instance of,
  #   e.g. :ip_address, :session_id
  # @return [RateLimiters::IpAddressRateLimiterService, RateLimiters::SessionIdRateLimiterService]
  def rate_limiter(rate_limiter_type)
    "RateLimiters::#{rate_limiter_type.to_s.camelize}RateLimiterService".constantize.new
  end

  # @param rate_limiter_type [Symbol] type of a rate limiter we want to create name of
  # @return [String] name of a rate limiter, e.g. 'IP ADDRESS'
  def rate_limiter_type_name(rate_limiter_type)
    rate_limiter_type.to_s.upcase.tr(*TR_ARGS)
  end
end
