# frozen_string_literal: true

Redis.current = Redis.new(url: Limiter::APP_CONFIG.limiter.redis.url)
