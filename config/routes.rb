# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'landing_page#index'

  resources :landing_page, only: [:index]
end
