# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LandingPageController do
  describe 'GET index' do
    let(:rate_limiters_caller) { instance_double('RateLimitersCallerService') }
    let(:status) { 'You have 5 IP ADDRESS & 3 SESSION ID type requests left yet.' }

    before do
      expect(RateLimitersCallerService)
        .to receive(:new)
        .and_return(rate_limiters_caller)
      expect(rate_limiters_caller)
        .to receive(:call)
        .with(request.remote_ip, session.id)
        .and_return(status)
    end

    it 'returns flash notice message with a status about limiting/banning' do
      get :index

      expect(flash[:notice]).to eq(status)
    end

    it 'renders the index template' do
      get :index

      expect(response).to render_template('index')
    end
  end
end
