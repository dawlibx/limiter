# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RateLimitersConfig do
  subject { RateLimitersConfig.instance }

  describe '#types' do
    it { expect(subject.types).to eq(%i[ip_address session_id]) }
  end
end
