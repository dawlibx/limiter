# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RateLimiters::IpAddressRateLimiterService do
  let(:ip_address) { Faker::Internet.ip_v4_address }
  let(:allowed_requests_number) do
    RateLimitersConfig.instance.config.ip_address.allowed_requests_number
  end

  describe '#call' do
    it 'returns a proper value after given \'n\' number of calls' do
      1.upto(allowed_requests_number) do |request_number|
        request_number_returned = subject.call(ip_address: ip_address)
        expect(request_number_returned).to eq(request_number)
      end

      # returns nil value when a given IP address is banned
      expect(subject.call(ip_address: ip_address)).to eq(nil)
    end
  end
end
