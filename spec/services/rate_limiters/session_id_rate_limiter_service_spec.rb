# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RateLimiters::SessionIdRateLimiterService do
  let(:ip_address) { Faker::Internet.ip_v4_address }
  let(:session_id) { Faker::Internet.password(32) }
  let(:allowed_requests_number) do
    RateLimitersConfig.instance.config.session_id.allowed_requests_number
  end

  describe '#call' do
    it 'returns zero value when there is nil provided as session ID' do
      expect(subject.call(ip_address: ip_address, session_id: nil)).to eq(0)
    end

    it 'returns a proper value after given \'n\' times of calls' do
      # returns a number of requests already made when not banned yet
      1.upto(allowed_requests_number) do |request_number|
        request_number_returned = subject.call(ip_address: ip_address, session_id: session_id)
        expect(request_number_returned).to eq(request_number)
      end

      # returns nil value when a given IP address is banned, after exceeding an allowed number of
      # requests to make
      expect(subject.call(ip_address: ip_address, session_id: session_id)).to eq(nil)
    end
  end
end
