# frozen_string_literal: true

require 'rails_helper'

# There is an assumption made that there are both IP address type and session ID type rate
# limiters enabled in the configuration.
RSpec.describe RateLimitersCallerService do
  let(:ip_address) { Faker::Internet.ip_v4_address }
  let(:session_id) { Faker::Internet.password(32) }
  let(:ip_address_rate_limiter) { instance_double('RateLimiters::IpAddressRateLimiterService') }
  let(:session_id_rate_limiter) { instance_double('RateLimiters::SessionIdRateLimiterService') }
  let(:ip_address_allowed_requests_number) do
    RateLimitersConfig.instance.config.ip_address.allowed_requests_number
  end
  let(:session_id_allowed_requests_number) do
    RateLimitersConfig.instance.config.session_id.allowed_requests_number
  end

  describe '#call' do
    before do
      expect(RateLimiters::IpAddressRateLimiterService)
        .to receive(:new)
        .and_return(ip_address_rate_limiter)
      expect(ip_address_rate_limiter)
        .to receive(:call)
        .with(ip_address: ip_address, session_id: session_id)
        .and_return(ip_address_requests_made_number)
      expect(RateLimiters::SessionIdRateLimiterService)
        .to receive(:new)
        .and_return(session_id_rate_limiter)
      expect(session_id_rate_limiter)
        .to receive(:call)
        .with(ip_address: ip_address, session_id: session_id)
        .and_return(session_id_requests_made_number)
    end

    context 'when there are both IP address and session ID limited' do
      let(:ip_address_requests_made_number) { 4 }
      let(:session_id_requests_made_number) { 2 }
      let(:ip_address_requests_left_number) do
        ip_address_allowed_requests_number - ip_address_requests_made_number
      end
      let(:session_id_requests_left_number) do
        session_id_allowed_requests_number - session_id_requests_made_number
      end

      it 'returns a message with information about limiting for all types of limiters' do
        expect(subject.call(ip_address, session_id)).to eq(
          "You have #{ip_address_requests_left_number} IP ADDRESS & " \
          "#{session_id_requests_left_number} SESSION ID type requests left yet."
        )
      end
    end

    context 'when there is IP address banned and session ID limited' do
      let(:ip_address_requests_made_number) { nil }
      let(:session_id_requests_made_number) { 1 }

      it 'returns a message with information about banning the IP address' do
        expect(subject.call(ip_address, session_id)).to eq('BANNED: IP ADDRESS type(s)!')
      end
    end

    context 'when there is IP address limited and session ID banned' do
      let(:ip_address_requests_made_number) { 1 }
      let(:session_id_requests_made_number) { nil }

      it 'returns a message with information about banning the session ID' do
        expect(subject.call(ip_address, session_id)).to eq('BANNED: SESSION ID type(s)!')
      end
    end

    context 'when there are both IP address and session ID banned' do
      let(:ip_address_requests_made_number) { nil }
      let(:session_id_requests_made_number) { nil }

      it 'returns a message with information about banning for all types of limiters' do
        expect(subject.call(ip_address, session_id))
          .to eq('BANNED: IP ADDRESS & SESSION ID type(s)!')
      end
    end
  end
end
